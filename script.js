$.get('https://jsonplaceholder.typicode.com/photos?_limit=10').done(function (data) {
    for (let i = 0; i < data.length; i++) {
        $('.row.start').append(`    <div class="col-6">
        <div class="row">
            <div class="col"><img src="${data[i].url}"></div>
            <div class="col">
                <p>${data[i].title}</p>
                <img src="${data[i].thumbnailUrl}">
            </div>
        </div>
    </div>`);
    }
});
